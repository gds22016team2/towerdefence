﻿using UnityEngine;
using System.Collections;

public class WeaponBuyButton : BuyButton {

	public GameObject weaponToBuy;
	/*
	private GameObject player; 

	// Use this for initialization

	void Start () {

		GameObject[] gos = GameObject.FindObjectsOfType(typeof(GameObject)) as GameObject[]; //will return an array of all GameObjects in the scene
		foreach(GameObject go in gos)
		{
			if(go.layer == 8  && go.CompareTag("Player"))
			{
				player = go; 
			}
		} 

	}*/
	
	// Update is called once per frame
	void Update () {
		WeaponManger weaponManager = player.GetComponent<WeaponManger>();
		if(weaponManager.GetComponent<WeaponManger>().ContainsWeapon(weaponToBuy.GetComponent<BaseWeapon>())){
			SetCanBuy(false);
		}
	}

	public void OnClick(){
		WeaponManger weaponManager = player.GetComponent<WeaponManger>();//GameObject.FindGameObjectWithTag ("Player").GetComponent<WeaponManger>();


		weaponManager.GetComponent<WeaponManger>().EquipWeapon (weaponToBuy.GetComponent<BaseWeapon>());
		base.OnMouseDown ();
	}

	/*
	protected override void OnMouseDown ()
	{
		GameObject weaponManager = GameObject.FindGameObjectWithTag ("WeaponManger");
		weaponManager.GetComponent<WeaponManger>().EquipWeapon (weaponToBuy.GetComponent<BaseWeapon>());
		base.OnMouseDown ();
	}
	*/
}
