﻿using UnityEngine;
using System.Collections;

public class BuildWeapon : BaseWeapon {

	//private GameObject player;
	public GameObject flame;
	public GameObject flameprefab;
	private Quaternion flameQuaternion;
	private Vector3 flameVector;

	private Transform parent;
	// Use this for initialization
	void Start () {

		parent = flame.transform.parent;
		flameVector = parent.position;
		flameQuaternion = parent.rotation;
		GameObject[] gos = GameObject.FindObjectsOfType(typeof(GameObject)) as GameObject[]; //will return an array of all GameObjects in the scene
		foreach(GameObject go in gos)
		{
			if(go.layer == 8 && go.CompareTag("Player"))//go.layer=="LocalPlayer" && go.CompareTag("Player"))
			{
				player = go; 
			}
		}

		gunAnim.Play ("Draw");

		player.GetComponent<GridInteraction> ().EnableGridInteraction (true);
	}

	void Awake(){
		GameObject[] gos = GameObject.FindObjectsOfType(typeof(GameObject)) as GameObject[]; //will return an array of all GameObjects in the scene
		foreach(GameObject go in gos)
		{
			if(go.layer == 8 && go.CompareTag("Player"))//go.layer=="LocalPlayer" && go.CompareTag("Player"))
			{
				player = go; 
			}
		}

		player.GetComponent<GridInteraction> ().EnableGridInteraction (true);
		gunAnim.Play ("Draw");
	}


	void OnDisable(){
		player.GetComponent<GridInteraction> ().EnableGridInteraction (false);
	}
	
	// Update is called once per frame
	void Update () {
		
		if (flame == null) {
			flame = (GameObject)Instantiate (flameprefab, new Vector3 (parent.position.x, parent.position.y + 0.03f, parent.position.z), parent.rotation, parent);
		} else {
			flame.transform.position = new Vector3 (parent.position.x, parent.position.y + 0.03f, parent.position.z);
			flame.transform.rotation = parent.rotation;
		}

		if (player == null) {
			GameObject[] gos = GameObject.FindObjectsOfType(typeof(GameObject)) as GameObject[]; //will return an array of all GameObjects in the scene
			foreach(GameObject go in gos)
			{
				if(go.layer == 8 && go.CompareTag("Player"))//go.layer=="LocalPlayer" && go.CompareTag("Player"))
				{
					player = go; 
				}
			}
		}

		if (!player.GetComponent<GridInteraction> ().gridInteractionEnabled) {
			player.GetComponent<GridInteraction> ().EnableGridInteraction (true);
		}

		if ((Input.GetButtonDown ("Fire1") || Input.GetMouseButtonDown (0))) {
			if (player.GetComponent<GridInteraction> ().BuildTower ()) { 
				gunAnim.Play ("Build");
			}

		}


		if ((Input.GetButtonDown ("Fire2") || Input.GetMouseButtonDown (1))) {

			if (player.GetComponent<GridInteraction> ().SellTower ()) {
				gunAnim.Play ("Destroy");
			}

		}

	
		if(Input.GetKeyDown(KeyCode.E)){
			player.GetComponent<GridInteraction> ().CycleTowerUp();
		}

		if(Input.GetKeyDown(KeyCode.Q)){
			player.GetComponent<GridInteraction> ().CycleTowerDown();
		}
	}
}
