﻿using UnityEngine;
using System.Collections;

public struct GridNode {
    private bool _mapNode;
    public bool mapNode {
        get { return _mapNode; }
        set { _mapNode = value; }
    }
    private bool _walkable;
	public bool walkable {
		get { return _walkable; }
		set { _walkable = value; }
	}
    private bool _flyable;
    public bool flyable {
        get { return _flyable; }
        set { _flyable = value; }
    }
    private bool _largeWalkable;
    public bool largeWalkable {
        get { return _largeWalkable; }
        set { _largeWalkable = value; }
    }
    private bool _largeFlyable;
    public bool largeFlyable {
        get { return _largeFlyable; }
        set { _largeFlyable = value; }
    }
    private Vector3 _worldPos;
	public Vector3 worldPos {
		get { return _worldPos; }
		set { _worldPos = value; }
	}

	public GridNode(bool mapNode, bool walkable, bool flyable, bool largeWalkable, bool largeFlyable, Vector3 worldPos){
        this._mapNode = mapNode;
		this._walkable = walkable;
        this._flyable = flyable;
        this._largeWalkable = largeWalkable; 
        this._largeFlyable = largeFlyable;
        this._worldPos = worldPos;
	}
    public GridNode(bool mapNode, bool walkable, bool flyable, Vector3 worldPos) {
        this._mapNode = mapNode;
        this._walkable = walkable;
        this._flyable = flyable;
        this._largeWalkable = true;
        this._largeFlyable = true;
        this._worldPos = worldPos;
    }
  
    public GridNode(bool walkable, bool flyable, Vector3 worldPos) {
        this._mapNode = false;
        this._walkable = walkable;
        this._flyable = flyable;
        this._largeWalkable = true;
        this._largeFlyable = true;
        this._worldPos = worldPos;
    }
    
    public bool GetWalkable(){
		return _walkable;
	}
	public void SetWalkable(bool w){
		walkable = w;
	}
	public static bool operator ==(GridNode gn1, GridNode gn2){
		return gn1.Equals (gn2);
	}
	public static bool operator !=(GridNode gn1, GridNode gn2){
		return !gn1.Equals (gn2);
	}
    public bool Equals(GridNode o) {
        return this._worldPos == o._worldPos;
    }
    public override bool Equals(object o) {
        return this.GetHashCode() == o.GetHashCode();
    }
    public static bool Equals(GridNode gn1, GridNode gn2) {
        return gn1.GetHashCode() == gn2.GetHashCode();
    }
    public override int GetHashCode() {
        return this._worldPos.GetHashCode();
    }
}
