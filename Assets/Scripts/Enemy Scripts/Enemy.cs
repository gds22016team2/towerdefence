﻿using UnityEngine;
using UnityEngine.Networking;
using System;
using System.Collections;
using System.Collections.Generic;

public class Enemy : BaseHealth {
    [SerializeField]
    protected List<Transform> edges;
    [SerializeField]
    public float explosionRange = 5;
    public const string PLACEHOLDER = "Placeholder";
	public const string FLYING = "Flying";
	public const string FASTFLYING = "FastFlying";
    [SerializeField]
    public string type;
	[SerializeField]//offsetToUpdatePath is how far the current target is away from the end of the current path in order to get a new path
	protected float WeaponDamage, WeaponRange, offsetToUpdatePath, moveSpeed, rotSpeed, viewDistance, hoverHeight, heightSpring; 
    public float weaponDamage {
        get { return WeaponDamage; }
    }
    public float weaponRange {
        get { return WeaponRange; }
    }
    public bool kamikaze;
    protected int currentPathIndex = 0;
    public int fundsWorth;
    protected bool canDeviate = true;
	protected Vector3 moveTarget;
	protected Vector3 targetOldPos;
    public bool groundUnit;
	protected Pathfinder pathfinder;
	protected Transform playerBase;
    public Vector3 targetPos;
    [SerializeField]
    protected float minSecsBetweenDeviations = 3;
    protected float deviationTimer = 3;
    [SerializeField]
    int deviationsAllowed = 2;
    [SerializeField]
    GameObject explosion;
	public AudioSource hummingSource;
	public AudioClip hummingSound;
	public AudioClip hummingDamageSound;
	public AudioSource damageSource;
	public AudioClip[] ricochetSounds;
	public AudioClip explosionSound;
    [SyncVar]
    protected Vector3 Pos;
    public Vector3 pos {
        get { return Pos; }
        set { Pos = value; }
    }
    public Vector3 dir;
    [SyncVar]
    protected Quaternion rot;
    public Vector3 standardPos;
    public ParticleSystem sparks;
	public ParticleSystem damageSparks;
    [SerializeField]
    protected LayerMask baseMask;
    [SerializeField]
    protected LayerMask playerMask;
    [SerializeField]
    protected LayerMask mapMask;
    protected bool onPath = false;
	public bool isDead = false;
    Vector3? sightedTargetPos;
    protected Rigidbody rb;
	public enum Target { PlayerBase, Player, Turret, Node }
    protected Target _target = Target.PlayerBase;
	public Target target {
		get { return _target; }
		set { _target = value; }
	}
    protected Vector3 oldClosestNode;
    protected Target oldTarget;
    protected float distanceToTarget;
    protected Vector3 oldTargetPos;
    protected Vector3 currentNodePos;
    protected float pathLastUpdated;
    protected int prevIndex = 0;
    public virtual void Start() {
		pathfinder = new Pathfinder();
		playerBase = GameObject.Find ("EndZone").transform;
		//UpdatePath ();
        rb = GetComponent<Rigidbody>();
        //  if(GameObject.FindGameObjectWithTag("InWall"))
        //Physics.IgnoreCollision (GetComponent<BoxCollider> (), GameObject.FindGameObjectWithTag ("InWall").GetComponent<BoxCollider>());
        rb.freezeRotation = true;
        rb.useGravity = false;
        deviationTimer = minSecsBetweenDeviations;
        pos = transform.position;
        rot = transform.rotation;
        hummingSource.clip = hummingSound;
		hummingSource.Play ();
	}


    Vector3? SightTargets(bool useWepRange) {
        if (!isServer) return null;
        float range = useWepRange ? weaponRange : viewDistance;
        Vector3? closestTarget = null;
        float leastDistance = 0;
        foreach (Transform edge in edges) {
            Vector3 edgePos = groundUnit ? EnemyManager.GroundY(edge.position) : EnemyManager.AirY(edge.position);
            Vector3 basePos = groundUnit ? EnemyManager.GroundY(playerBase.transform.position) : EnemyManager.AirY(playerBase.transform.position);
            Vector3 dir = basePos - edgePos;

            dir.Normalize();
            RaycastHit hit;
            if (Physics.Raycast(edgePos, dir, out hit, range, baseMask)) {
                if (hit.transform.tag == "Map") {
                    closestTarget = null;
                    leastDistance = 0;
                    break;
                } else {
                    if (hit.transform.CompareTag("Base")) {
                        target = Target.PlayerBase;
                        closestTarget = groundUnit ? EnemyManager.GroundY(playerBase.transform.position) : EnemyManager.AirY(playerBase.transform.position);
                        leastDistance = Vector3.Distance((Vector3)closestTarget, pos);
                    }
                }
            }
        }
        foreach (Player player in GameManager.players.Values) {
            bool broke = false;
            foreach (Transform edge in edges) {
                Vector3 edgePos = groundUnit ? EnemyManager.GroundY(edge.position) : EnemyManager.AirY(edge.position);
                Vector3 playerPos = groundUnit ? EnemyManager.GroundY(player.transform.position) : EnemyManager.AirY(player.transform.position);
                Vector3 dir = playerPos - edgePos;

                dir.Normalize();
                RaycastHit hit;
                if (Physics.Raycast(edgePos, dir, out hit, range, playerMask)) {
                    if (hit.transform.tag == "Map") {
                        broke = true;
                        break;
                    } else {
                        Vector3 groundedHitPos = groundUnit ? EnemyManager.GroundY(hit.transform.position) : EnemyManager.AirY(hit.transform.position);
                        float distance = Vector3.Distance(groundedHitPos, standardPos);
                        if (hit.transform.root.gameObject.GetComponent<Player>()) {
                            if (closestTarget != null) {
                                if (distance < leastDistance) {
                                    target = Target.Player;
                                    closestTarget = groundedHitPos;
                                    leastDistance = distance;
                                }
                            } else {
                                target = Target.Player;
                                closestTarget = groundedHitPos;
                                leastDistance = distance;
                            }
                        }
                    }
                }
            }
            if (broke) break;
        }
        return closestTarget;
    }
    protected virtual void Update() {
        //this must be first \/
        if (!isServer) {
            //transform.position = Vector3.Lerp(transform.position, pos, 0.11f);
            //transform.rotation = Quaternion.Lerp(transform.rotation, rot, 0.11f);
              
            pathLastUpdated += Time.deltaTime;
            if (currentHealth <= maxHealth / 2) {
                if (hummingSource.clip.GetInstanceID() != hummingDamageSound.GetInstanceID()) {
                    hummingSource.Stop();
                    hummingSource.clip = hummingDamageSound;
                    hummingSource.Play();
                }

                if (!damageSparks.isPlaying) {
                    damageSparks.Play();
                }
            }
            return;
        }
        if (GameManager.singleton.GameOver()) {
            print("SELF DESTRUCT");
            Die();
        }
        if (onPath && !canDeviate && deviationTimer < minSecsBetweenDeviations) {
            deviationTimer += Time.deltaTime;
        }
        //if (Input.GetKey(KeyCode.UpArrow)) {
        //    pos += Vector3.up * 0.1f;
        //}
    }
    bool AbleToShoot() {
        if (kamikaze) return false;
        if (sightedTargetPos != null) {
            targetPos =(Vector3) sightedTargetPos;
            Rotate();
            return true;
        }
        return false;
    }

    protected virtual void FixedUpdate() {
        //This has to be first \/
        if (!isServer) {
            transform.position = Vector3.Lerp(transform.position, pos, 0.11f);
            transform.rotation = Quaternion.Lerp(transform.rotation, rot, 0.11f);
            return;
        }
            //TODO maybe make it so if enemy follows player out of sight of the path the enemy immediately breaks off back to the path

            sightedTargetPos = SightTargets(true);
        if (!AbleToShoot()) {
            if (groundUnit) {
                standardPos = EnemyManager.GroundY(pos);
                Navigate(EnemyManager.singleton.groundHeight);
            } else {
                standardPos = EnemyManager.AirY(pos);
                Navigate(EnemyManager.singleton.airHeight);

            }
            Rotate();
            distanceToTarget = EnemyManager.HorizontalDistance(standardPos, targetPos);
            Move();
            distanceToTarget = EnemyManager.HorizontalDistance(standardPos, targetPos);
            if (kamikaze && (target == Target.Player || target == Target.PlayerBase) && distanceToTarget < weaponRange) {
                Die();
            }
        }
    }
    protected void Navigate(float height) {

        Vector3? newClosestNode = EnemyManager.GetClosestPathNodePos(standardPos, edges, mapMask, groundUnit);
        if (newClosestNode != null) {
            if (EnemyManager.HorizontalDistance(pos, (Vector3)newClosestNode) >Grid.nodeDiameter*1.5f) {
                onPath = false;
            }
        }
        List<Vector3> path = groundUnit ? EnemyManager.groundPath : EnemyManager.airPath;
        if (!onPath) {

            if (target != Target.Node) {
                target = Target.Node;
                if (newClosestNode != null) {
                    targetPos = (Vector3)newClosestNode;
                } else {
                    targetPos = oldClosestNode;
                }
                currentPathIndex = EnemyManager.GetPathIndexFromPos(targetPos, offsetToUpdatePath, groundUnit);
            } else if (target == Target.Node && distanceToTarget < offsetToUpdatePath) {
                onPath = true;
            }
        }
        if (onPath) {
            //if (!canDeviate && deviationsAllowed > 0 && deviationTimer >= minSecsBetweenDeviations) {
            //    canDeviate = true;
            //    deviationsAllowed--;
            //}
            //if (canDeviate && sightedTargetPos != null) {
            //    onPath = false;
            //    targetPos = (Vector3)sightedTargetPos;
            //    canDeviate = false;
            //    deviationTimer = 0;
            //} else 
            if (distanceToTarget < offsetToUpdatePath) {
                target = Target.Node;
                if(path.Count -1 > currentPathIndex)
                    currentPathIndex++;
                if (groundUnit && EnemyManager.groundPath.Count > currentPathIndex + 1) {
                    targetPos = EnemyManager.groundPath[currentPathIndex];
                    oldClosestNode = targetPos;
                } else if (!groundUnit && EnemyManager.airPath.Count > currentPathIndex + 1) {
                    targetPos = EnemyManager.airPath[currentPathIndex];
                    oldClosestNode = targetPos;
                } else {
                    Debug.LogError("Out of range of path. PathIndex: " + currentPathIndex + " groundPath.Count: " + EnemyManager.groundPath.Count);
                }
            }
        }
        oldClosestNode =(Vector3)newClosestNode;
    }
    protected void Move() {
        float yDivergence = Mathf.Abs(pos.y - hoverHeight);
        if (yDivergence != 0)
            transform.position = new Vector3(transform.position.x, hoverHeight, transform.position.z);
        // rb.AddForce(Vector3.MoveTowards(pos, new Vector3(pos.x, hoverHeight, pos.z), Mathf.Pow(heightSpring, yDivergence)) );
       
        Vector3 dir = (EnemyManager.GroundY(targetPos) - EnemyManager.GroundY(transform.position));
        dir.Normalize();
        rb.AddForce(dir * moveSpeed);

        
        pos = transform.position;
    }
    
    protected void Rotate() {
        Vector3 targetDir = targetPos - standardPos;
        targetDir.Normalize();
        targetDir = EnemyManager.RemoveY(targetDir);
        dir = targetDir;
        rot = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(targetDir), rotSpeed);
        transform.rotation = rot;
    }
   
    protected override void Die() {
        if (isServer) {
            Explode();

            Enemy enemy = default(Enemy);
            string ID = "";
            foreach (KeyValuePair<string, Enemy> kp in EnemyManager.enemies) {
                if (kp.Value.pos == pos) {
                    enemy = kp.Value;
                    ID = kp.Key;
                    break;
                }
            }
            if (enemy != default(Enemy)) {
                EnemyManager.enemies.Remove(ID);
            }
            base.Die();

        }
    }
    protected void OnCollisionEnter(Collision collision) {
        if (!kamikaze) return;
        foreach (ContactPoint contact in collision.contacts) {
            if (contact.otherCollider.transform.name == "EndZone") {
                Die();
                return;
            } else if (contact.otherCollider.transform.name.Contains("Player")) {
                Die();
                return;
            }
        }
    }
    protected virtual void Explode() {
        GameObject exp = (GameObject)GameObject.Instantiate(explosion, pos, new Quaternion());
        exp.GetComponent<Explosion>().enemy = this;
        NetworkServer.Spawn(exp);
        isDead = true;
    }

    public override void TakeDamage(float _dmg) {
        sparks.Play();

        //For playing sounds only.
        int i = UnityEngine.Random.Range(0, ricochetSounds.Length);

        AudioClip clip = ricochetSounds[i];
        damageSource.Stop();
        damageSource.clip = clip;
        damageSource.Play();
        base.TakeDamage(_dmg);
    }
   
}
