﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
public class Explosion : NetworkBehaviour {
    public float explosionForce = 4;
    public float multiplier = 1;
    [HideInInspector]
    public Enemy enemy;

    private IEnumerator Start() {
        // wait one frame because some explosions instantiate debris which should then
        // be pushed by physics force
        yield return null;
        
        Multiplier();

        float r = enemy.explosionRange * multiplier;
        Collider[] cols = Physics.OverlapSphere(enemy.pos, r);
        List<Rigidbody> rigidbodies = new List<Rigidbody>();
        foreach (Collider col in cols) {
            if (isServer) {
                bool hasHealth = col.GetComponent<BaseHealth>() ? true : false;
                if (hasHealth) {
                    col.GetComponent<BaseHealth>().TakeDamage(enemy.weaponDamage);
                }
            }
           
        }
    }


    private void Multiplier() {
        var systems = GetComponentsInChildren<ParticleSystem>();
        foreach (ParticleSystem system in systems) {
            system.startSize *= multiplier;
            system.startSpeed *= multiplier;
            system.startLifetime *= Mathf.Lerp(multiplier, 1, 0.5f);
            system.Clear();
            system.Play();
        }

        Destroy(gameObject, 3.0f); //TODO come back to see this - explode death egix fix
    }
}
